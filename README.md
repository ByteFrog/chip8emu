# Chip8Emu

A CHIP-8 interpreter written in C++ using SDL2.

# Motivation

I've always been fascinated by how computers run underneath all the convenience of higher-level languages (and in this case, I mean anything higher than byte code). Writing a CHIP-8 interpreter seemed like a fun way to put my knowledge of computer systems to the test and explore the concepts behind interpreters/emulators.

# Requirements

SDL2

# How to Use

There's no makefile included with this project (yet), so you'll need to compile it manually from the source code. See the requirements for which libraries you'll need.

This interpreter is run from the command-line using the command:
    Chip8Emu program_name

Where the program name is the file name of the CHIP-8 program to run (usually ending with a .ch8 if downloaded from the internet).
You can find a variety of CHIP-8 programs [here](https://github.com/dmatlack/chip8). Note that some programs may cause the interpreter to crash if they attempt to access resources that aren't available to the interpreter. This is expected behaviour.

# Credits

A huge thanks to Frédéric Devernay for making a selection of very useful technical documents available on his website regarding CHIP-8 interpreters. You can find his website [here](http://devernay.free.fr/) and a more direct link to the CHIP-8 documentation [here](http://devernay.free.fr/hacks/chip8/).
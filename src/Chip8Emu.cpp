/*
 * Chip8Emu.cpp
 *
 *  Created on: Apr. 23, 2020
 *      Author: Matthew Virdaeus
 */

#include "Chip8Emu.h"

#include <stdexcept>
#include <iostream>
#include <fstream>
#include <cmath>

void audioCallback(void* userData, Uint8* stream, int len) {
	static_cast<Chip8Emu*>(userData)->generateTone(stream, len);
}

Chip8Emu::Chip8Emu(std::string filePath)
: i_(0), dt_(0), st_(0), pc_(0x200), sp_(0),
  window_(NULL), renderer_(NULL), backBuffer_(NULL),
  randomDistribution_(0, 0xFF), soundDevice_(0), sampleNum_(0) {

	std::string errorMsg;

	//Video initialization
	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0) {
		errorMsg = std::string("Failed to initialize SDL. SDL_Error:\n") + SDL_GetError();
		throw std::runtime_error(errorMsg);
	}

	window_ = SDL_CreateWindow("Chip8Emu", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 800, 400, SDL_WINDOW_RESIZABLE);
	if(window_ == NULL) {
		errorMsg = std::string("Failed to create window. SDL_Error:\n") + SDL_GetError();
		throw std::runtime_error(errorMsg);
	}

	renderer_ = SDL_CreateRenderer(window_, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC | SDL_RENDERER_TARGETTEXTURE);
	if(renderer_ == NULL) {
		errorMsg = std::string("Failed to create renderer. SDL_Error:\n") + SDL_GetError();
		throw std::runtime_error(errorMsg);
	}

	backBuffer_ = SDL_CreateTexture(renderer_, SDL_GetWindowPixelFormat(window_), SDL_TEXTUREACCESS_TARGET, 64, 32);
	if(backBuffer_ == NULL) {
		errorMsg = std::string("Failed to create back buffer texture. SDL_Error:\n") + SDL_GetError();
		throw std::runtime_error(errorMsg);
	}

	//Audio initialization
	SDL_AudioSpec audioWant, audioHave;
	SDL_zero(audioWant);
	audioWant.freq = AUDIO_SAMPLE_RATE;
	audioWant.format = AUDIO_S8;
	audioWant.channels = 1;
	audioWant.samples = 1024;
	audioWant.callback = audioCallback;
	audioWant.userdata = this;

	soundDevice_ = SDL_OpenAudioDevice(NULL, 0, &audioWant, &audioHave, 0);
	if(soundDevice_ <= 0) {
		errorMsg = std::string("Failed to open audio. SDL_Error:\n") + SDL_GetError();
		throw std::runtime_error(errorMsg);
	}

	//Load the data from the file into the program memory space starting at 0x200.
	std::ifstream file(filePath, std::ifstream::binary);
	if(!file.is_open()) {
		errorMsg = std::string("Could not open file.");
		throw std::runtime_error(errorMsg);
	}
	char byte;
	int pos = 0x200;
	while(file.get(byte)) {
		if(pos < 0xFFF) {
			memory_[pos] = byte;
			++pos;
		}
		else {
			errorMsg = std::string("Program exceeds maximum size.");
			throw std::runtime_error(errorMsg);
		}
	}

	//Place the system font sprites in the interpreter memory space (since the interpreter doesn't need it)
	//0
	memory_[0x000] = 0xF0;
	memory_[0x001] = 0x90;
	memory_[0x002] = 0x90;
	memory_[0x003] = 0x90;
	memory_[0x004] = 0xF0;
	//1
	memory_[0x005] = 0x20;
	memory_[0x006] = 0x60;
	memory_[0x007] = 0x20;
	memory_[0x008] = 0x20;
	memory_[0x009] = 0x70;
	//2
	memory_[0x00A] = 0xF0;
	memory_[0x00B] = 0x10;
	memory_[0x00C] = 0xF0;
	memory_[0x00D] = 0x80;
	memory_[0x00E] = 0xF0;
	//3
	memory_[0x00F] = 0xF0;
	memory_[0x010] = 0x10;
	memory_[0x011] = 0xF0;
	memory_[0x012] = 0x10;
	memory_[0x013] = 0xF0;
	//4
	memory_[0x014] = 0x90;
	memory_[0x015] = 0x90;
	memory_[0x016] = 0xF0;
	memory_[0x017] = 0x10;
	memory_[0x018] = 0x10;
	//5
	memory_[0x019] = 0xF0;
	memory_[0x01A] = 0x80;
	memory_[0x01B] = 0xF0;
	memory_[0x01C] = 0x10;
	memory_[0x01D] = 0xF0;
	//6
	memory_[0x01E] = 0xF0;
	memory_[0x01F] = 0x80;
	memory_[0x020] = 0xF0;
	memory_[0x021] = 0x90;
	memory_[0x022] = 0xF0;
	//7
	memory_[0x023] = 0xF0;
	memory_[0x024] = 0x10;
	memory_[0x025] = 0x20;
	memory_[0x026] = 0x40;
	memory_[0x027] = 0x40;
	//8
	memory_[0x028] = 0xF0;
	memory_[0x029] = 0x90;
	memory_[0x02A] = 0xF0;
	memory_[0x02B] = 0x90;
	memory_[0x02C] = 0xF0;
	//9
	memory_[0x02D] = 0xF0;
	memory_[0x02E] = 0x90;
	memory_[0x02F] = 0xF0;
	memory_[0x030] = 0x10;
	memory_[0x031] = 0xF0;
	//A
	memory_[0x032] = 0xF0;
	memory_[0x033] = 0x90;
	memory_[0x034] = 0xF0;
	memory_[0x035] = 0x90;
	memory_[0x036] = 0x90;
	//B
	memory_[0x037] = 0xE0;
	memory_[0x038] = 0x90;
	memory_[0x039] = 0xE0;
	memory_[0x03A] = 0x90;
	memory_[0x03B] = 0xE0;
	//C
	memory_[0x03C] = 0xF0;
	memory_[0x03D] = 0x80;
	memory_[0x03E] = 0x80;
	memory_[0x03F] = 0x80;
	memory_[0x040] = 0xF0;
	//D
	memory_[0x041] = 0xE0;
	memory_[0x042] = 0x90;
	memory_[0x043] = 0x90;
	memory_[0x044] = 0x90;
	memory_[0x045] = 0xE0;
	//E
	memory_[0x046] = 0xF0;
	memory_[0x047] = 0x80;
	memory_[0x048] = 0xF0;
	memory_[0x049] = 0x80;
	memory_[0x04A] = 0xF0;
	//F
	memory_[0x04B] = 0xF0;
	memory_[0x04C] = 0x80;
	memory_[0x04D] = 0xF0;
	memory_[0x04E] = 0x80;
	memory_[0x04F] = 0x80;

	//Clear the screen memory of garbage.
	clearScreen();

	//Fit the render area to the screen using the default dimensions, otherwise nothing renders.
	scaleRenderArea(800, 400);

	//DEBUG

	//DEBUG END

}

Chip8Emu::~Chip8Emu() {
	if(backBuffer_ != NULL) {
		SDL_DestroyTexture(backBuffer_);
	}
	if(renderer_ != NULL) {
		SDL_DestroyRenderer(renderer_);
	}
	if(window_ != NULL) {
		SDL_DestroyWindow(window_);
	}

	SDL_Quit();
}

void Chip8Emu::run() {
	SDL_Event event;
	while(true) {
		unsigned int startTime = SDL_GetTicks();
		while(SDL_PollEvent(&event) != 0) {
			switch(event.type) {
			case SDL_QUIT:
				goto quit;
				break;
			case SDL_WINDOWEVENT:
				if(event.window.event == SDL_WINDOWEVENT_SIZE_CHANGED) {
					scaleRenderArea(event.window.data1, event.window.data2);
				}
				break;
			}
		}
		updateButtons();

		//Because the emulator is supposed to run at 540 Hz, but the main loop runs at
		//60 Hz, we need to run 9 instructions per loop (540 / 60 = 9). We won't use a
		//loop so we can avoid having to run the check each iteration (small performance
		//increase).
		/*
		for(int i = 0; i < 9; ++i) {
			execute();
		}
		*/
		execute();
		execute();
		execute();
		execute();
		execute();
		execute();
		execute();
		execute();
		execute();

		if(dt_ > 0) {
			--dt_;
		}

		if(st_ > 0) {
			SDL_PauseAudioDevice(soundDevice_, 0);
			--st_;
		}
		else {
			SDL_PauseAudioDevice(soundDevice_, 1);
		}

		render();

		unsigned int endTime = SDL_GetTicks();
		if(endTime < startTime + MS_PER_CYCLE) {
			SDL_Delay(startTime + MS_PER_CYCLE - endTime);
		}
	}
	quit:
	return;
}

//Executes the instruction currently pointed at by the program counter and advances the program counter.
void Chip8Emu::execute() {
	unsigned short opcode = (memory_[pc_] << 8) | memory_[pc_+1];
	switch(opcode & 0xF000) {
	case 0x0000:
		switch(opcode) {
		//0x00E0 - CLS [O]
		case 0x00E0: {
			clearScreen();
			pc_+= 2;
		}
		break;
		//0x00EE - RET [O]
		case 0x00EE: {
			pc_ = stack_[sp_-1] + 2;
			--sp_;
		}
		break;
		}
	break;
	//1nnn - JP addr [O]
	case 0x1000: {
		pc_ = opcode & 0x0FFF;
	}
	break;
	//2nnn - CALL addr [O]
	case 0x2000: {
		stack_[sp_] = pc_;
		++sp_;
		pc_ = opcode & 0x0FFF;
	}
	break;
	//3xkk - SE Vx, byte [O]
	case 0x3000: {
		if(v_[(opcode & 0x0F00) >> 8] == (opcode & 0x00FF)) {
			pc_ += 4;
		}
		else {
			pc_ += 2;
		}
	}
	break;
	//4xkk - SNE Vx, byte [O]
	case 0x4000: {
		if(v_[(opcode & 0x0F00) >> 8] != (opcode & 0x00FF)) {
			pc_ += 4;
		}
		else {
			pc_ += 2;
		}
	}
	break;
	//5vx0 - SE Vx, Vy
	case 0x5000: {
		if(v_[(opcode & 0x0F00) >> 8] == v_[(opcode & 0x00F0) >> 4]) {
			pc_ += 4;
		}
		else {
			pc_ += 2;
		}
	}
	break;
	//6xkk - LD Vx, byte [O]
	case 0x6000: {
		v_[(opcode & 0x0F00) >> 8] = (opcode & 0x00FF);
		pc_ += 2;
	}
	break;
	//7xkk - ADD Vx, byte [O]
	case 0x7000: {
		v_[(opcode & 0x0F00) >> 8] += (opcode & 0x00FF);
		pc_ += 2;
	}
	break;
	case 0x8000: {
		switch(opcode & 0x000F) {
		//8xy0 - LD Vx, Vy
		case 0x0000: {
			v_[(opcode & 0x0F00) >> 8] = v_[(opcode & 0x00F0) >> 4];
			pc_ += 2;
		}
		break;
		//8xy1 - OR Vx, Vy
		case 0x0001: {
			v_[(opcode & 0x0F00) >> 8] |= v_[(opcode & 0x00F0) >> 4];
			pc_ += 2;
		}
		break;
		//8xy2 - AND Vx, Vy [O]
		case 0x0002: {
			v_[(opcode & 0x0F00) >> 8] &= v_[(opcode & 0x00F0) >> 4];
			pc_ += 2;
		}
		break;
		//8xy3 - XOR Vx, Vy
		case 0x0003: {
			v_[(opcode & 0x0F00) >> 8] ^= v_[(opcode & 0x00F0) >> 4];
			pc_ += 2;
		}
		break;
		//8xy4 - ADD Vx, Vy
		case 0x0004: {
			unsigned short temp = v_[(opcode & 0x0F00) >> 8] + v_[(opcode & 0x00F0) >> 4];
			if(temp > 0xFF) {
				v_[0xF] = 0x1;
			}
			v_[(opcode & 0x0F00) >> 8] += v_[(opcode & 0x00F0) >> 4];
			pc_ += 2;
		}
		break;
		//8xy5 - SUB Vx, Vy
		case 0x0005: {
			if(v_[(opcode & 0x0F00) >> 8] > v_[(opcode & 0x00F0) >> 4]) {
				v_[0xF] = 0x1;
			}
			v_[(opcode & 0x0F00) >> 8] -= v_[(opcode & 0x00F0) >> 4];
			pc_ += 2;
		}
		break;
		//8xy6 - SHR Vx {, Vy}
		case 0x0006: {
			v_[0xF] = v_[(opcode & 0x0F00) >> 8] & 0x1;
			v_[(opcode & 0x0F00) >> 8] >>= 1;
			pc_ += 2;
		}
		break;
		//8xy7 - SUBN Vx, Vy
		case 0x0007: {
			if(v_[(opcode & 0x0F00) >> 8] < v_[(opcode & 0x00F0) >> 4]) {
				v_[0xF] = 0x1;
			}
			v_[(opcode & 0x0F00) >> 8] = v_[(opcode & 0x00F0) >> 4] - v_[(opcode & 0x0F00) >> 8];
			pc_ += 2;
		}
		break;
		//8xyE - SHL Vx {, Vy}
		case 0x000E: {
			v_[0xF] = (v_[(opcode & 0x0F00) >> 8] & 0x8) >> 3;
			v_[(opcode & 0x0F00) >> 8] <<= 1;
			pc_ += 2;
		}
		break;
		}
	}
	break;
	//9xy0 - SNE Vx, Vy
	case 0x9000: {
		if(v_[(opcode & 0x0F00) >> 8] != v_[(opcode & 0x00F0) >> 4]) {
			pc_ += 4;
		}
		else {
			pc_ += 2;
		}
	}
	break;
	//Annn - LD I, addr [O]
	case 0xA000: {
		i_ = opcode & 0x0FFF;
		pc_ += 2;
	}
	break;
	//Bnnn - JP V0, addr
	case 0xB000: {
		pc_ = (opcode & 0x0FFF) + v_[0x0];
	}
	break;
	//Cxkk - RND Vx, byte [O]
	case 0xC000: {
		v_[(opcode & 0x0F00) >> 8] = randomDistribution_(randomGenerator_) & (opcode & 0x00FF);
		pc_ += 2;
	}
	break;
	//Dxyn - DRW Vx, Vy, nibble [?]
	case 0xD000: {
		unsigned char x1 = v_[(opcode & 0x0F00) >> 8] / 8;
		unsigned char x2 = x1 + 1;
		if(x1 == 64) {
			x2 = 0;
		}
		unsigned char xOffset = v_[(opcode & 0x0F00) >> 8] - (8 * x1);
		unsigned char y = v_[(opcode & 0x00F0) >> 4];
		v_[0xF] = 0;

		for(unsigned char n = 0; n < (opcode & 0x000F); ++n) {
			unsigned char s1 = memory_[i_+n] >> xOffset;
			unsigned char s2 = memory_[i_+n] << (8 - xOffset);
			if((s1 & screen_[x1][y]) != 0 || (s2 & screen_[x2][y]) != 0) {
				v_[0xF] = 1;
			}
			screen_[x1][y] ^= s1;
			screen_[x2][y] ^= s2;
			++y;
			if(y == 32) {
				y = 0;
			}
		}
		pc_ += 2;
	}
	break;
	case 0xE000: {
		switch(opcode & 0x00FF) {
		//Ex9E - SKP Vx
		case 0x009E: {
			if(v_[(opcode & 0x0F00) >> 8] <= 0x0F) {
				if(((0x1 << v_[(opcode & 0x0F00) >> 8]) & buttons_) != 0) {
					pc_ += 2;
				}
			}
			pc_ += 2;
		}
		break;
		//ExA1 - SKNP Vx [O]
		case 0x00A1: {
			if(v_[(opcode & 0x0F00) >> 8] <= 0x0F) {
				if(((0x1 << v_[(opcode & 0x0F00) >> 8]) & buttons_) == 0) {
					pc_ += 2;
				}
			}
			pc_ += 2;
		}
		break;
		}
	}
	break;
	case 0xF000: {
		switch(opcode & 0x00FF) {
		//Fx07 - LD Vx, DT
		case 0x0007: {
			v_[(opcode & 0x0F00) >> 8] = dt_;
			pc_ += 2;
		}
		break;
		//Fx0A - LD Vx, K [O]
		case 0x000A: {
			if(buttons_ != 0) {
				for(unsigned char n = 0; n < 0xF; ++n) {
					if((buttons_ & 0x1) != 0) {
						v_[(opcode & 0x0F00) >> 8] = n;
						pc_ += 2;
						break;
					}
					buttons_ >>= 1;
				}
			}
		}
		break;
		//Fx15 - LD DT, Vx
		case 0x0015: {
			dt_ = v_[(opcode & 0x0F00) >> 8];
			pc_ += 2;
		}
		break;
		//Fx18 - LD ST, Vx [O]
		case 0x0018: {
			st_ = v_[(opcode & 0x0F00) >> 8];
			pc_ += 2;
		}
		break;
		//Fx1E - ADD I, Vx
		case 0x001E: {
			i_ = i_ + v_[(opcode & 0x0F00) >> 8];
			pc_ += 2;
		}
		break;
		//Fx29 - LD F, Vx
		case 0x0029: {
			i_ = v_[(opcode & 0x0F00) >> 8] * 5;
			pc_ += 2;
		}
		break;
		//Fx33 - LD B Vx
		case 0x0033: {
			memory_[i_] = v_[(opcode & 0x0F00) >> 8] / 100;
			memory_[i_+1] = (v_[(opcode & 0x0F00) >> 8] - memory_[i_]) / 10;
			memory_[i_+2] = v_[(opcode & 0x0F00) >> 8] - memory_[i_] - memory_[i_+1];
			pc_ += 2;
		}
		break;
		//Fx55 - LD [I], Vx
		case 0x0055: {
			for(unsigned char n = 0; n <= v_[(opcode & 0x0F00) >> 8] && n <= 0xF; ++n) {
				memory_[i_+n] = v_[n];
			}
			pc_ += 2;
		}
		break;
		//Fx65 - LD Vx, [I]
		case 0x0065: {
			for(unsigned char n = 0; n <= v_[(opcode & 0x0F00) >> 8] && n <= 0xF; ++n) {
				v_[n] = memory_[i_+n];
			}
			pc_ += 2;
		}
		break;
		}
	}
	break;
	}
}

void Chip8Emu::render() {
	//Clear the back buffer.
	SDL_SetRenderTarget(renderer_, backBuffer_);
	SDL_SetRenderDrawColor(renderer_, 0, 0, 0, 255);
	SDL_RenderClear(renderer_);

	//Draw pixels to the back buffer.
	SDL_SetRenderDrawColor(renderer_, 255, 255, 255, 255);
	for(int y = 0; y < 32; ++y) {
		for(int x = 0; x < 8; ++x) {
			for(int n = 0; n < 8; ++n) {
				if(((0x1 << (7 - n)) & screen_[x][y]) != 0) {
					SDL_RenderDrawPoint(renderer_, (x * 8) + n, y);
				}
			}
		}
	}

	//Copy the back buffer to the screen.
	SDL_SetRenderTarget(renderer_, NULL);
	SDL_SetRenderDrawColor(renderer_, 0, 0, 255, 255);
	SDL_RenderClear(renderer_);
	SDL_RenderCopy(renderer_, backBuffer_, NULL, &renderArea_);
	SDL_RenderPresent(renderer_);
}

void Chip8Emu::updateButtons() {
	buttons_ = 0;
	const Uint8* keyboardState = SDL_GetKeyboardState(NULL);
	if(keyboardState[SDL_SCANCODE_COMMA]) {
		buttons_ |= 0x0001;
	}
	if(keyboardState[SDL_SCANCODE_7]) {
		buttons_ |= 0x0002;
	}
	if(keyboardState[SDL_SCANCODE_8]) {
		buttons_ |= 0x0004;
	}
	if(keyboardState[SDL_SCANCODE_9]) {
		buttons_ |= 0x0008;
	}
	if(keyboardState[SDL_SCANCODE_U]) {
		buttons_ |= 0x0010;
	}
	if(keyboardState[SDL_SCANCODE_I]) {
		buttons_ |= 0x0020;
	}
	if(keyboardState[SDL_SCANCODE_O]) {
		buttons_ |= 0x0040;
	}
	if(keyboardState[SDL_SCANCODE_J]) {
		buttons_ |= 0x0080;
	}
	if(keyboardState[SDL_SCANCODE_K]) {
		buttons_ |= 0x0100;
	}
	if(keyboardState[SDL_SCANCODE_L]) {
		buttons_ |= 0x0200;
	}
	if(keyboardState[SDL_SCANCODE_M]) {
		buttons_ |= 0x0400;
	}
	if(keyboardState[SDL_SCANCODE_PERIOD]) {
		buttons_ |= 0x0800;
	}
	if(keyboardState[SDL_SCANCODE_0]) {
		buttons_ |= 0x1000;
	}
	if(keyboardState[SDL_SCANCODE_P]) {
		buttons_ |= 0x2000;
	}
	if(keyboardState[SDL_SCANCODE_SEMICOLON]) {
		buttons_ |= 0x4000;
	}
	if(keyboardState[SDL_SCANCODE_SLASH]) {
		buttons_ |= 0x8000;
	}
}

void Chip8Emu::scaleRenderArea(unsigned int width, unsigned int height) {
	double newAspectRatio = (double)width / (double)height;
	//The window is wider than the render area. Letterbox the sides.
	if(newAspectRatio > 2) {
		renderArea_.h = height;
		renderArea_.w = height * 2;
		renderArea_.y = 0;
		renderArea_.x = (width - renderArea_.w) / 2;
	}
	//The window is taller than the render area. Letterbox the top and bottom.
	else {
		renderArea_.w = width;
		renderArea_.h = width / 2;
		renderArea_.x = 0;
		renderArea_.y = (height - renderArea_.h) / 2;
	}
}

void Chip8Emu::clearScreen() {
	for(int y = 0; y < 32; ++y) {
		for(int x = 0; x < 8; ++x) {
			screen_[x][y] = 0;
		}
	}
}

void Chip8Emu::generateTone(Uint8 *stream, int len) {
	Sint8* buffer = reinterpret_cast<Sint8*>(stream);

	for(int i = 0; i < len; ++i, ++sampleNum_) {
		double time = (double)sampleNum_ / (double)AUDIO_SAMPLE_RATE;
		buffer[i] = (Sint8)(AUDIO_AMPLITUDE * sin(2.0f * 3.14159 * 300.0f * time));
	}
}

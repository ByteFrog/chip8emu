/*
 * Chip8Emu.h
 *
 *  Created on: Apr. 23, 2020
 *      Author: Matthew Virdaeus
 *
 *  Notes:
 *  - Most CHIP-8 interpreters ran at ~500-540Hz. I've chosen 540 Hz for this project because having
 *    a frequency that's a multiple of 60 Hz should make coding the delay and sound timers easier.
 */

#pragma once

#include <SDL2/SDL.h>
#include <string>
#include <random>

class Chip8Emu {
public:
	Chip8Emu(std::string filePath);
	virtual ~Chip8Emu();

	void run();
	void generateTone(Uint8* stream, int len);

private:
	//CHIP-8 variables
	unsigned char memory_[4096];
	unsigned char v_[16];
	unsigned short stack_[16];
	unsigned short i_;
	unsigned char dt_;
	unsigned char st_;
	unsigned short pc_;
	unsigned char sp_;
	//The standard screen for Chip8 is 64x32. Since it's monochrome, we only need 1 bit per pixel.
	unsigned char screen_[8][32];
	//A bit-field for tracking which buttons are pressed. LSB = 0, MSB = F.
	unsigned short buttons_;

	//Emulation variables
	SDL_Window* window_;
	SDL_Renderer* renderer_;
	SDL_Texture* backBuffer_;
	SDL_Rect renderArea_;
	std::uniform_int_distribution<unsigned char> randomDistribution_;
	std::default_random_engine randomGenerator_;
	SDL_AudioDeviceID audioDevice_;

	const unsigned int MS_PER_CYCLE = 17;
	const int AUDIO_AMPLITUDE = 28000;
	const int AUDIO_SAMPLE_RATE = 44100;
	SDL_AudioDeviceID soundDevice_;
	int sampleNum_;

	void execute();
	void render();
	void updateButtons();
	void scaleRenderArea(unsigned int width, unsigned int height);
	void clearScreen();
};

/*
 * main.cpp
 *
 *  Created on: Apr. 23, 2020
 *      Author: Matthew Virdaeus
 */

#include "Chip8Emu.h"

#include <iostream>
#include <string>

int main(int argc, char* argv[]) {
	if(argc == 2) {
		Chip8Emu chip8(argv[1]);
		chip8.run();
	}
	else {
		std::cout << "Usage: Chip8Emu program_file" << std::endl;
	}
	return 0;
}


